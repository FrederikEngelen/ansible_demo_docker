# Ansible Db2 demo

A docker-compose demo on using Ansible with Db2

This example has one Ansible control node controlling 2 Db2 nodes.

By default, it will store the database files under /opt/db2data_* (also on Windows 10 with WSL2). Modify `docker-compose.yml` to change this behaviour. 

## Quickstart

* Build: `make build`
* Start demo servers: `make start`
* Start Ansible prompt: `docker exec -ti ansible_control_node sh`
* Stop demo: `make clean`

After starting, give the Db2 nodes some time to start up SSH. You can use the connectivity test for that purpose.

## Examples

* Test the connectivity to the hosts
```
ansible db2_hosts -m ping
```

* Run ad-hoc db2pd command on one instance
```
ansible lab1_db2_insts -m shell -a 'db2pd -'
```

* Run the complete environment playbook
```
ansible-playbook demo.yml
```

## Etc

Any changes in the ansible directory will be visible inside the control node server. Edit them from the host, don't bother playing around with them inside the container.

The security configuration as used should only be used for demo environments.

Some inspiration (make) was taken from https://github.com/ltamaster.

## Small print

THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


